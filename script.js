// Global
const buttons = document.querySelectorAll('.button');
let output = document.querySelector('#output');
let mathFunctions = {
    '+': function (a, b) { return a + b; },
    '-': function (a, b) { return a - b; },
    '*': function (a, b) { return a * b; },
    '/': function (a, b) { return a / b; }
}

// Clear-btn Handler
document.querySelector('#clear').addEventListener('click', () => {
    output.innerHTML = '';
});

// Symbol-buttons callbacks
for (button of buttons) {
    button.addEventListener('click', (event) => {
        const symbol = event.currentTarget.innerHTML;
        let outValue = output.innerHTML;

        // Check the symbol. If it is [*/+-] - the Check prevent doubling it in Output
        if (checkSymbol(symbol))
            if (checkSymbol(outValue[outValue.length - 1])) {
                output.innerHTML = outValue.substring(0, outValue.length - 1) + symbol;
                return;
            }
        ////////
        symbol == '=' ? output.innerHTML = calculation(output.innerHTML) : output.innerHTML += symbol;
        ///////
    });
}

// Check type of symbol
function checkSymbol(symbol) {
    if (isNaN(parseInt(symbol)) && symbol != '.' && symbol != '=')
        return true;
}

// Expression Handler and calculations.
function calculation(expression) {
    // Check the expression for the last symbol (isNaN or not)
    if (checkSymbol(expression[expression.length - 1]))
        expression = expression.substring(0, expression.length - 1);
    
    let exprArray = splitExpressionToArray(expression);

    // Math operations. Type of operation as argument to function
    while (exprArray.length != 1) {
        if (exprArray.includes('*'))
            calcSplice(exprArray, '*');
        else if (exprArray.includes('/'))
            calcSplice(exprArray, '/');
        else if (exprArray.includes('+'))
            calcSplice(exprArray, '+');
        else if (exprArray.includes('-'))
            calcSplice(exprArray, '-');
    }
    return exprArray.join();
}

// Get math operator and two sibling values and calculate it
function calcSplice(exprArray, symbol) {
    let index = exprArray.indexOf(symbol);
    let num1 = +exprArray[index - 1];
    let num2 = +exprArray[index + 1];
    exprArray.splice(index - 1, index + 2, mathFunctions[symbol](num1, num2));
    return exprArray;
}




// Parse the Expression to Math-symbols and numbers in array
function splitExpressionToArray(expression) {
    let exprArray = [];
    let temp = '';

    for (let i = 0; i < expression.length; i++) {
        // Check all symbols isNaN and split numbers and math-symbols in one array
        // For example the string '11+21*7/90' returns array ['11','+','21','*','7','/','90',]...

        if (!isNaN(parseInt(expression[i])) || expression[i] == '.') {
            temp = temp + expression[i];
        }
        else {
            exprArray.push(temp);
            exprArray.push(expression[i])
            temp = '';
        }
        if (i == expression.length - 1) exprArray.push(temp);
    }
    return exprArray;
}